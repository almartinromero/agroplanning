# agroplanning

# Comandos necesarios para usar git
- git add . -> Para añadir todos los archivos modificados o nuevos
- git status -> Este comando muestra la lista de los archivos que se han cambiado junto con los archivos que están por ser añadidos o comprometidos.
- git commit -m "Mensaje para el commit" -> El comando commit es usado para cambiar a la cabecera. Ten en cuenta que cualquier cambio comprometido no afectara al repertorio remoto

- git checkout -b <nombre-rama> -> El comando checkout se puede usar para crear ramas o cambiar entre ellas, si añades -b, crea la rama, sino, simplemente se cambia a la rama.
- git push origin <nombre-rama> -> Este es uno de los comandos más básicos. Un simple push envía los cambios que se han hecho en la rama principal de los repertorios remotos que están asociados con el directorio que está trabajando.
- git pull origin <nombre-rama> -> Para poder fusionar todos los cambios que se han hecho en el repositorio local trabajando


